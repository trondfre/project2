import os
from numpy import *
from matplotlib.pyplot import *

for omega_r in (0.01, 0.5, 1, 5):
#    os.system('./project2.x %s' % omega_r)
    
    non_int_data = loadtxt("values_non_interaction_omega_r=%s.dat" % omega_r)
    int_data = loadtxt("values_interaction_omega_r=%s.dat" % omega_r)
    
    figure()
    subplot(3,1,1)
    plot(non_int_data[:,0], non_int_data[:,1]**2, int_data[:,0], int_data[:,1]**2)
    legend(['without interaction', 'with interaction'])
    title(r'probability distribution $|\psi(\rho)|^2$, $\omega_r =$ %s' % omega_r)
    ylabel('ground state')

    subplot(3,1,2)
    plot(non_int_data[:,0], non_int_data[:,2]**2, int_data[:,0], int_data[:,2]**2)
    legend(['without interaction', 'with interaction'])
    xlabel(r'$\rho$')
    ylabel('first excited state')

    subplot(3,1,3)
    plot(non_int_data[:,0], non_int_data[:,3]**2, int_data[:,0], int_data[:,3]**2)
    legend(['without interaction', 'with interaction'])
    xlabel(r'$\rho$')
    ylabel('second exited state')

    omega_r = str(omega_r)
    savefig("wavefunction_omega_r=%s.pdf" % omega_r.replace('.', ','))

show()
