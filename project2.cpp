#include <iostream>
#include <cmath>
#include <armadillo>
#include <time.h>
#include <sstream>

using namespace std;
using namespace arma;

double maxoffdiag(mat &A, int &k, int &l, int n)
//finds the maximum offdiagonal matrix element, A[k][l]
{
  double max = 0;

  for (int i=0; i<n; i++){
    for (int j=i+1; j<n; j++){
      if (fabs(A(i,j)) > max){
	max = fabs(A(i,j));
	l = i;
	k = j;
      }
    }
  }
  return max;
} // end of function maxoffdiag

void rotate(mat &A, int &k, int &l, int n)
//performs similarity transformations on matrix A to make it diagonal
{
  double s, c;
  if (A(k,l) != 0.0){
    double t, tau;
    tau = (A(l,l) - A(k,k))/(2*A(k,l));
    if (tau > 0 ){
      t = 1.0/(tau + sqrt(1.0 + tau*tau));
    }
    else{
      t = -1.0/(-tau + sqrt(1.0 + tau*tau));
    }

    c = 1/sqrt(1+t*t);
    s = c*t;
  }
  else{
    c = 1.0;
    s = 0.0;
  }
  double a_kk, a_ll, a_ik, a_il;
  a_kk = A(k,k);
  a_ll = A(l,l);
  //changing the matrix elements
  A(k,k) = c*c*a_kk - 2.0*c*s*A(k,l) + s*s*a_ll;
  A(l,l) = s*s*a_kk + 2.0*c*s*A(k,l) + c*c*a_ll;
  A(k,l) = 0.0;
  A(l,k) = 0.0;

  for (int i=0; i<n; i++){
    if (i != k && i != l){
      a_ik = A(i,k);
      a_il = A(i,l);
      A(i,k) = c*a_ik - s*a_il;
      A(k,i) = A(i,k);
      A(i,l) = c*a_il + s*a_ik;
      A(l,i) = A(i,l);
    }
  }
  return;
} //end of function rotate

int main(int argc, char* argv[])
{

  int n = 200, k, l;
  double rho[n+2], rho_max = 5, epsilon = 1.0e-10, omega_r = atof(argv[1]);
  double max_number_iterations = (double) n * (double) n * (double) n;
  int iterations = 0;

  //B contains the interaction term
  mat A = zeros(n,n), B = zeros(n,n);
  vec offdiag(n-1), diagonal(n), diagonal_int(n);
  
  rho[0] = 0;
  rho[n+1] = rho_max;
  double h = rho_max/n;

  for (int i=1; i<=n; i++){
    rho[i] = i*h;
    diagonal[i-1] = 2./(h*h) + omega_r*rho[i]*rho[i];
    diagonal_int[i-1] = 2./(h*h) + omega_r*rho[i]*rho[i] + 1./rho[i];
  }
  A.diag() = diagonal;
  A.diag(1) = offdiag.fill(-1/(h*h));
  A.diag(-1) = offdiag;

  B.diag() = diagonal_int;
  B.diag(1) = offdiag.fill(-1/(h*h));
  B.diag(-1) = offdiag;

  //compute eigenvalues and vectors of A with armadillo and take time
  vec arma_eigval, arma_eigval_int;
  mat arma_eigvec, arma_eigvec_int;
  clock_t start, finish;
  start = clock();
   
  eig_sym(arma_eigval, arma_eigvec, A);

  finish = clock();
  double armadillo_time = (double)(finish - start)/CLOCKS_PER_SEC;

  eig_sym(arma_eigval_int, arma_eigvec_int, B);


  double max_offdiag = maxoffdiag(A, k, l, n);

  //diagonalize A and take time
  start = clock();

  while ( max_offdiag > epsilon && iterations <= max_number_iterations ){
    max_offdiag = maxoffdiag(A, k, l, n);
    rotate(A, k, l, n);
    iterations++;
  }

  finish = clock();
  double jacobi_time = (double)(finish - start)/CLOCKS_PER_SEC;

  max_offdiag = maxoffdiag(B, k, l, n);

  //diagonalize B
  while ( max_offdiag > epsilon && iterations <= max_number_iterations ){
    max_offdiag = maxoffdiag(B, k, l, n);
    rotate(B, k, l, n);
    iterations++;
  }


  //sort eigenvalues of A and B
  vec eigenvals(n), temp(n), eigenvals_int(n), temp_int(n);
  for (int i=0; i<n; i++){
    temp(i) = A(i,i);
    temp_int(i) = B(i,i);
  }
  eigenvals = sort(temp);
  eigenvals_int = sort(temp_int);

  cout << "omega_r = " << omega_r << ":" << endl;
  cout << "Needed " << iterations << " transformations to reach tolerance of " << epsilon << endl << endl;
  cout << "The three lowest eigenvalues using Jacobi's method: " << eigenvals(0)  << ", " << eigenvals(1) << ", " << eigenvals(2) << endl;
  cout << "time used by Jacobi's method: " << jacobi_time << " seconds" << endl << endl;
  cout << "Same eigenvalues using armadillo: " << arma_eigval(0)  << ", " << arma_eigval(1) << ", " << arma_eigval(2) << endl;
  cout << "time used by armadillo: " << armadillo_time << " seconds" << endl << endl;

  cout << "The three lowest eigenvalues with interaction term: " << eigenvals_int(0)  << ", " << eigenvals_int(1) << ", " << eigenvals_int(2) << endl << endl;

     //save to file
    ostringstream omega_string;
    omega_string << omega_r; //convert number to string
    string filename = "values_non_interaction_omega_r=" + omega_string.str() + ".dat";
    const char *filename_non_int = filename.c_str();
    filename = "values_interaction_omega_r=" + omega_string.str() + ".dat";
    const char *filename_int = filename.c_str();

    FILE *output_file_non_int, *output_file_int;
    output_file_non_int = fopen(filename_non_int, "w");
    output_file_int = fopen(filename_int, "w");
    for (int i=0; i<n; i++){
      fprintf(output_file_non_int, "%12.5E %12.5E %12.5E %12.5E \n", rho[i+1], arma_eigvec(i,0), arma_eigvec(i,1), arma_eigvec(i,2));
      fprintf(output_file_int, "%12.5E %12.5E %12.5E %12.5E \n", rho[i+1], arma_eigvec_int(i,0), arma_eigvec_int(i,1), arma_eigvec_int(i,2));
    }
    fclose(output_file_non_int);
    fclose(output_file_int);


  return 0;
} //end of function main
